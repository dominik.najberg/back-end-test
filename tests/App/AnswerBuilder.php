<?php declare(strict_types=1);

namespace Tests\App;

use App\Domain\Answer;
use App\Domain\Survey;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class AnswerBuilder
{
    private UuidInterface $id;
    private int $quality = 0;
    private ?string $comment = null;
    private ?Survey $survey  = null;

    private function __construct()
    {
        $this->id = Uuid::uuid4();
    }

    public static function new(): self
    {
        return new self();
    }

    public function withQuality(int $quality): self
    {
        $this->quality = $quality;

        return $this;
    }

    public function withComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function build(): Answer
    {
        return new Answer(
            $this->id,
            $this->quality,
            $this->comment,
            $this->survey,
        );
    }
}
