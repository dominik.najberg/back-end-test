<?php declare(strict_types=1);

namespace Tests\App;

use PHPUnit\Framework\TestCase;

class AnswerTest extends TestCase
{
    // choosing Poor or Very Poor (-1 or -2) makes comment required as an explanation of choice
    public function test_answer_has_quality_set(): void
    {
        $answer = AnswerBuilder::new()
            ->withQuality(-1)
            ->build();

        self::assertIsInt($answer->quality);
    }
}
