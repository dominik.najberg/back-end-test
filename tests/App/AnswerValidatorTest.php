<?php declare(strict_types=1);

namespace Tests\App;

use App\Domain\AnswerValidator;
use App\Domain\Exception\AnswerException;
use PHPUnit\Framework\TestCase;

class AnswerValidatorTest extends TestCase
{
    /**
     * @dataProvider data_provider_for_test_comment_required_on_low_quality
     * @param int[] $qualities
     */
    public function test_comment_required_on_low_quality(array $qualities, string $message, ?string $comment): void
    {
        foreach ($qualities as $quality) {
            $answer = AnswerBuilder::new()
                ->withComment($comment)
                ->withQuality($quality)
                ->build();

            $this->expectException(AnswerException::class);
            $this->expectExceptionMessage($message);

            AnswerValidator::validateComment($answer);
        }
    }

    /**
     * @return array<string, array{
     *     quality: array<int, int>,
     *     message: string,
     *     comment: ?string
     * }>
     */
    public static function data_provider_for_test_comment_required_on_low_quality(): array
    {
        return [
            'poor quality' => [
                'quality' => [-1, -2],
                'message' => 'Comment is required for poor quality',
                'comment' => null,
            ],
            'good quality' => [
                'quality' => [0, 1, 2],
                'message' => 'There should be no comment when good quality',
                'comment' => 'That is a comment',
            ],
        ];
    }
}
