<?php

declare(strict_types=1);

namespace Tests\App;

use App\Domain\Exception\SurveyException;
use App\Domain\Survey;
use PHPUnit\Framework\TestCase;

final class SurveyTest extends TestCase
{
    public function test_survey_does_not_accept_answer_when_not_live(): void
    {
        $answer = AnswerBuilder::new()->build();
        $survey = new Survey();

        $this->expectException(SurveyException::class);

        $survey->addAnswer($answer);
    }

    public function test_survey_accepts_answer_when_live(): void
    {
        $answer = AnswerBuilder::new()->build();
        $expectedAnswers = [$answer];
        $survey = new Survey();
        $survey->goLive();

        $survey->addAnswer($answer);

        self::assertSame($expectedAnswers, $survey->getAnswers());
    }

    public function test_only_new_survey_can_be_edited(): void
    {
        $survey = new Survey();
        $expectedName = 'expected name';
        $survey->setName($expectedName);
        self::assertSame($expectedName, $survey->getName());

        $survey->goLive();
        $this->expectException(SurveyException::class);
        $survey->setName('Any name');
    }

    public function test_cannot_add_answer_to_closed_survey(): void
    {
        $answer = AnswerBuilder::new()->build();
        $survey = new Survey();
        $survey->close();

        $this->expectException(SurveyException::class);
        $survey->addAnswer($answer);
    }
}
