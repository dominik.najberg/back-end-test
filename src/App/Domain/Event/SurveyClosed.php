<?php declare(strict_types=1);

namespace App\Domain\Event;

use Ramsey\Uuid\UuidInterface;

class SurveyClosed
{
    public function __construct(
        public readonly UuidInterface $surveyId,
    ){
    }
}
