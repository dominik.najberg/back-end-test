<?php

namespace App\Domain;

use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class Answer
{
    public function __construct(
        public readonly UuidInterface $id,
        public readonly int $quality,
        public readonly ?string $comment = null,
        public ?Survey $survey = null,
    ) {
    }

    public function addSurvey(Survey $survey): void
    {
        $this->survey = $survey;
    }
}
