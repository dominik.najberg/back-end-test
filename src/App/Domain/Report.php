<?php

namespace App\Domain;

use DateTimeImmutable;
use Ramsey\Uuid\UuidInterface;

class Report
{
    public function __construct(
        private ?UuidInterface $id = null,
        private ?int $numberOfAnswers = null,
        private ?int $quality = null,
        private array $comments = [],
        private ?DateTimeImmutable $generatedAt = null,
        private ?Survey $survey = null,
    ){
    }
}
