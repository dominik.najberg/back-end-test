<?php declare(strict_types=1);

namespace App\Domain;

use App\Domain\Exception\AnswerException;

class AnswerValidator
{
    public static function validateComment(Answer $answer): void
    {
        if ($answer->comment === null && in_array($answer->quality, [-2, -1], true)) {
            throw new AnswerException('Comment is required for poor quality');
        }

        if ($answer->comment !== null && in_array($answer->quality, [0, 1, 2], true)) {
            throw new AnswerException('There should be no comment when good quality');
        }
    }
}
