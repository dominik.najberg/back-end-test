<?php

namespace App\Domain;

use App\Domain\Exception\SurveyException;
use Ramsey\Uuid\UuidInterface;

class Survey
{
    public const STATUS_NEW = 'new';
    public const STATUS_LIVE = 'live';
    public const STATUS_CLOSED = 'closed';

    public function __construct(
        public UuidInterface $id,
        public string $name,
        public string $status,
        public array $answers = [],
        public ?Report $report = null,
        public ?string $reportEmail = null,
    ) {
        $this->status = self::STATUS_NEW;
    }

    public function setName(string $name): self
    {
        $this->assertSurveyNew();

        $this->name = $name;

        return $this;
    }

    public function addAnswer(Answer $answer): self
    {
        $this->assertSurveyLive();

        if (!array_filter($this->answers, static function ($item) {
            return $item instanceof Answer;
        })) {
            $this->answers[] = $answer;
            $answer->addSurvey($this);
        }

        return $this;
    }

    public function goLive(): void
    {
        $this->status = self::STATUS_LIVE;
    }

    public function close(): void
    {
        $this->status = self::STATUS_CLOSED;
    }

    private function assertSurveyLive(): void
    {
        if ($this->status !== self::STATUS_LIVE) {
            throw new SurveyException();
        }
    }

    private function assertSurveyNew(): void
    {
        if ($this->status !== self::STATUS_NEW) {
            throw new SurveyException();
        }
    }
}
