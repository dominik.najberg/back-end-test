<?php

declare(strict_types=1);

namespace App\UI\Http;

use App\Application\Command\CreateSurvey;
use App\Application\MessageBus\CommandBus;
use App\Application\MessageBus\QueryBus;
use App\Application\Query\GetAllSurveys;
use App\Application\Report\ReportGenerator;
use App\Application\Report\ReportMailer;
use App\Domain\Survey;
use App\Infrastructure\Repository\DoctrineSurveyRepository;
use App\Infrastructure\Security\Voter\SurveyVoter;
use App\UI\Http\Form\CreateSurveyType;
use App\UI\Http\Form\StatusType;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SurveyController extends AbstractController
{
    public function __construct(
        private readonly ReportGenerator          $reportGenerator,
        private readonly ReportMailer             $reportMailer,
        private readonly DoctrineSurveyRepository $surveyRepository,
        private readonly CommandBus $commandBus,
        private readonly QueryBus $queryBus,
    ) {
    }

    public function index(): JsonResponse
    {
        return $this->json(
            $this->queryBus->query(new GetAllSurveys()),
        );
    }

    public function create(Request $request): JsonResponse
    {
        $form = $this->createForm(CreateSurveyType::class);
        $form->submit(json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR));

        if ($form->isSubmitted() && $form->isValid()) {
            $formData = $form->getData();

            $id = Uuid::uuid4();
            $createSurvey = new CreateSurvey(
                $id,
                $formData['name'],
                $formData['status'],
                $formData['answers'] ?? [],
                $formData['report'] ?? null,
                $formData['reportEmail'] ?? null
            );

            $this->commandBus->dispatch($createSurvey);
        } else {
            return $this->json($form);
        }

        return $this->json([
            'id' => $id->toString(),
        ], Response::HTTP_CREATED);
    }

    // We need better and more precise use cases here
    public function edit(Survey $survey, Request $request): JsonResponse
    {
        $this->denyAccessUnlessGranted(SurveyVoter::EDIT, $survey);

        return $this->handleRequest($survey, $request);
    }

    // TODO use a command instead
    public function delete(Survey $survey): JsonResponse
    {
        $this->denyAccessUnlessGranted(SurveyVoter::DELETE, $survey);
        $this->surveyRepository->remove($survey);

        return $this->json('', Response::HTTP_NO_CONTENT);
    }

    private function handleRequest(Survey $survey, Request $request): JsonResponse
    {
        $form = $this->createForm(CreateSurveyType::class, $survey);
        $form->submit(json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR));

        if ($form->isSubmitted() && $form->isValid()) {
            $this->surveyRepository->save($survey);
        } else {
            return $this->json($form);
        }

        return $this->json($survey);
    }

    // TODO use concrete commands instead ($survey->close)
    public function changeStatus(Survey $survey, Request $request): JsonResponse
    {
        $form = $this->createForm(StatusType::class);
        $form->submit(json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR));
        if ($form->isSubmitted() && $form->isValid()) {
            $survey->setStatus($form->getData()['status']);


            if ($survey->getStatus() === Survey::STATUS_CLOSED) {
                $report = $this->reportGenerator->generate($survey);
                $this->reportMailer->send($report);
            }

            $this->surveyRepository->save($survey, true);
        } else {
            return $this->json($form);
        }

        return $this->json($survey);
    }
}
