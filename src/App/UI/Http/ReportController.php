<?php

declare(strict_types=1);

namespace App\UI\Http;

use App\Domain\Report;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class ReportController extends AbstractController
{
    public function show(Report $report): JsonResponse
    {
        return $this->json($report);
    }
}
