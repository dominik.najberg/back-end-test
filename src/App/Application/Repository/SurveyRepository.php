<?php

namespace App\Application\Repository;
use App\Domain\Survey;
use Ramsey\Uuid\UuidInterface;

interface SurveyRepository
{
    public function get(UuidInterface $id): Survey;
    public function save(Survey $entity): void;
    public function remove(Survey $entity): void;
}
