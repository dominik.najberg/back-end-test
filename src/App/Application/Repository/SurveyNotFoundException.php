<?php declare(strict_types=1);

namespace App\Application\Repository;

// Simplified, can contain more info
class SurveyNotFoundException extends \Exception
{
}
