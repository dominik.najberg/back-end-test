<?php

namespace App\Application\Repository;
use App\Domain\Report;

interface ReportRepository
{
    public function save(Report $entity): void;
    public function remove(Report $entity): void;
}
