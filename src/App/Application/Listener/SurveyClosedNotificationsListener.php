<?php declare(strict_types=1);

namespace App\Application\Listener;

use App\Application\Report\ReportGenerator;
use App\Application\Report\ReportMailer;
use App\Application\Repository\SurveyRepository;
use App\Domain\Event\SurveyClosed;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class SurveyClosedNotificationsListener implements MessageHandlerInterface
{
    public function __construct(
        private readonly ReportGenerator $reportGenerator,
        private readonly ReportMailer $reportMailer,
        private readonly SurveyRepository $surveyRepository,
    ){
    }

    public function __invoke(SurveyClosed $surveyClosed): void
    {
        $survey = $this->surveyRepository->get($surveyClosed->surveyId);
        $report = $this->reportGenerator->generate($survey);
        $this->reportMailer->send($report);
    }
}
