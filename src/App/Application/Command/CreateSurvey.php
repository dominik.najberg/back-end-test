<?php declare(strict_types=1);

namespace App\Application\Command;

use App\Domain\Report;
use Ramsey\Uuid\UuidInterface;

class CreateSurvey
{
    public function __construct(
        public UuidInterface $id,
        public string $name,
        public string $status,
        public array $answers = [],
        public ?Report $report = null,
        public ?string $reportEmail = null,
    ){
    }
}
