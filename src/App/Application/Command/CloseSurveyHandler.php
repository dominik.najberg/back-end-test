<?php declare(strict_types=1);

namespace App\Application\Command;

use App\Domain\Event\SurveyClosed;
use App\Domain\Exception\SurveyException;
use App\Infrastructure\Repository\DoctrineSurveyRepository;
use Symfony\Component\Messenger\MessageBusInterface;

class CloseSurveyHandler
{
    public function __construct(
        public readonly DoctrineSurveyRepository $surveyRepository,
        public readonly MessageBusInterface      $messageBus,
    ){
    }

    public function __invoke(CloseSurvey $closeSurvey)
    {
        $survey = $this->surveyRepository->find($closeSurvey->surveyId);
        if (null === $survey) {
            throw new SurveyException('survey not found');
        }
        $survey->close();

        $this->messageBus->dispatch(new SurveyClosed($survey->getId()));
    }
}
