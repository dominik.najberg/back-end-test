<?php declare(strict_types=1);

namespace App\Application\Command;

use Ramsey\Uuid\UuidInterface;

class CloseSurvey
{
    public function __construct(
        public readonly UuidInterface $surveyId,
    ){
    }
}
