<?php declare(strict_types=1);

namespace App\Application\Query;

use App\Application\Repository\SurveyRepository;

class GetAllSurveysQueryHandler
{
    public function __construct(
        public readonly SurveyRepository $surveyRepository
    ){
    }

    public function __invoke(GetAllSurveys $getAllSurveys): iterable
    {
        return $this->surveyRepository->findAll();
    }
}
