<?php

declare(strict_types=1);

namespace App\Application\Report;

use App\Application\Repository\ReportRepository;
use App\Domain\Report;
use App\Domain\Survey;
use Ramsey\Uuid\Uuid;

final class ReportGenerator
{
    public function __construct(
        private readonly ReportRepository $reportRepository
    ){
    }

    public function generate(Survey $survey): Report
    {
        $sum = 0;
        $counter = 0;
        $comments = [];
        foreach ($survey->answers as $answer) {
            $sum += (int) $answer->getQuality();
            if ($answer->getComment() !== null) {
                $comments[] = $answer->getComment();
            }

            $counter++;
        }

        $report = new Report(
            Uuid::uuid4(),
            $counter,
            (int) ($sum / $counter),
            $comments,
            new \DateTimeImmutable(),
            $survey,
        );

        $this->reportRepository->save($report);

        return $report;
    }
}
