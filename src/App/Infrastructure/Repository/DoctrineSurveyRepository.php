<?php

namespace App\Infrastructure\Repository;

use App\Application\Repository\SurveyNotFoundException;
use App\Application\Repository\SurveyRepository;
use App\Domain\Survey;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Ramsey\Uuid\UuidInterface;

class DoctrineSurveyRepository extends ServiceEntityRepository implements SurveyRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Survey::class);
    }

    /**
     * @throws SurveyNotFoundException
     */
    public function get(UuidInterface $id): Survey
    {
        $survey = $this->find($id);

        if (null === $survey) {
            throw new SurveyNotFoundException();
        }
    }

    // Flush done in DoctrineFlushSubscriber
    public function save(Survey $entity): void
    {
        $this->getEntityManager()->persist($entity);
    }

    public function remove(Survey $entity): void
    {
        $this->getEntityManager()->remove($entity);
    }
}
