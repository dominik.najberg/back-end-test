<?php

namespace App\Infrastructure\Repository;

use App\Application\Repository\ReportRepository;
use App\Domain\Report;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class DoctrineReportRepository extends ServiceEntityRepository implements ReportRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Report::class);
    }

    public function save(Report $entity): void
    {
        $this->getEntityManager()->persist($entity);
    }

    public function remove(Report $entity): void
    {
        $this->getEntityManager()->remove($entity);
    }
}
